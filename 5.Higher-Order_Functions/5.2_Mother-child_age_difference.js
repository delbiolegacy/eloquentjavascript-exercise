/* Proposta del delbio */
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});

// Your code here.

function motherChildAgeDifference(person){
  return person.born - byName[person.mother].born;
}
function isMotherDefined(person){
  return byName[person.mother] !== undefined;
}

console.log(average(ancestry.filter(isMotherDefined).map(motherChildAgeDifference.bind(null))));

// → 31.2

/* proposta del libro */
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});

var differences = ancestry.filter(function(person) {
  return byName[person.mother] != null;
}).map(function(person) {
  return person.born - byName[person.mother].born;
});

console.log(average(differences));
// → 31.2
