/* proposta dal delbio */
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}
function age(p) { return p.died - p.born; }
function century(p) { return Math.ceil(p.died / 100); }

function groupBy(array, f){
  var grouped = {};
  array.forEach(function(person){
      var groupBy = f(person);
      if (!Array.isArray(grouped[groupBy])){
        grouped[groupBy] = [];
      }
      grouped[groupBy].push(age(person));
      /* la seguente riga e' il risultato
       * dopo la vista della soluzione,
       * la lascio commentata per ricordare che non la avevo pensata
       */
       //grouped[groupBy].push(person);
  });
  return grouped;
}

var groupedBy = groupBy(ancestry, century);
var avgGroupedBy = {};
for(var group in groupedBy){
  avgGroupedBy[group] = average(groupedBy[group])
  /* la seguente riga e' il risultato
   * dopo la vista della soluzione,
   * la lascio commentata per ricordare che non la avevo pensata
   */
  //avgGroupedBy[group] = average(groupedBy[group].map(age));
}

console.log(avgGroupedBy)

// Your code here.

// → 16: 43.5
//   17: 51.2
//   18: 52.8
//   19: 54.8
//   20: 84.7
//   21: 94

/* proposta dal libro */
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

function groupBy(array, groupOf) {
  var groups = {};
  array.forEach(function(element) {
    var groupName = groupOf(element);
    if (groupName in groups)
      groups[groupName].push(element);
    else
      groups[groupName] = [element];
  });
  return groups;
}

var byCentury = groupBy(ancestry, function(person) {
  return Math.ceil(person.died / 100);
});

for (var century in byCentury) {
  var ages = byCentury[century].map(function(person) {
    return person.died - person.born;
  });
  console.log(century + ": " + average(ages));
}

// → 16: 43.5
//   17: 51.2
//   18: 52.8
//   19: 54.8
//   20: 84.7
//   21: 94
