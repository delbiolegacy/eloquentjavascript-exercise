/* Proposta dal Delbio */
var arrays = [[1, 2, 3], [4, 5], [6]];
console.log(arrays.reduce(function(a, b) {
  return a.concat(b);
}));

// → [1, 2, 3, 4, 5, 6]

/* Proposta dal libro */
var arrays = [[1, 2, 3], [4, 5], [6]];

console.log(arrays.reduce(function(flat, current) {
  return flat.concat(current);
}, []));

// → [1, 2, 3, 4, 5, 6]
