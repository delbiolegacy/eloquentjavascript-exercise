
/* Proposta di delbio */

var chessBoard = '';
var characters = [' ', '#'];
var size = 16;
for ( var n = 0; n < size; n++){
  var i = n % 2;
  for (var line = ''; line.length < size ; i++){
    line += characters[i % 2];
  }
  chessBoard += line + "\n";
}
console.log(chessBoard);

/* Proposta del libro */

var size = 8;

var board = "";

for (var y = 0; y < size; y++) {
  for (var x = 0; x < size; x++) {
    if ((x + y) % 2 == 0)
      board += " ";
    else
      board += "#";
  }
  board += "\n";
}

console.log(board);
