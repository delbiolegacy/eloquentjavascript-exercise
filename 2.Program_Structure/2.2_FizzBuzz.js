
/* Proposta di delbio */

for (var n = 1 ; n <= 100; n++){
  var isDivisibleBy3 = n % 3 == 0;
  var isDivisibleBy5 = n % 5 == 0;

  if (isDivisibleBy3 && isDivisibleBy5) console.log("FizzBuzz");
  else if (isDivisibleBy3) console.log("Fizz");
  else if (isDivisibleBy5) console.log("Buzz");
  else console.log(n);
}

/* Proposta del libro */

for (var n = 1; n <= 100; n++) {
  var output = "";
  if (n % 3 == 0)
    output += "Fizz";
  if (n % 5 == 0)
    output += "Buzz";
  console.log(output || n);
}
