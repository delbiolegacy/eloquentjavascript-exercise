/* Proposta del Delbio */
<!doctype html>
<script src="code/chapter/15_game.js"></script>
<script src="code/game_levels.js"></script>

<link rel="stylesheet" href="css/game.css">

<body>
<script>
  // The old runGame function. Modify it...
  function runGame(plans, Display) {
    var startLives = 3;
    var firstLevel = 0;

    function startLevel(n, lives) {
      console.log('level: ', n, 'lives: ', lives);
      runLevel(new Level(plans[n]), Display, function(status) {
        if (lives === 0)
          restartGame();
        else if (status == "lost")
          startLevel(n, lives -1);
        else if (n < plans.length - 1)
          startLevel(n + 1, lives);
        else
          console.log("You win!");
      });
    }
    function restartGame(){
      console.log('Game Over, ..., Restart from level '+firstLevel+' ;)');
      startLevel(firstLevel, startLives)
    }
    startLevel(firstLevel, startLives);
  }
  runGame(GAME_LEVELS, DOMDisplay);
</script>
</body>

/* proposta del libro */
<!doctype html>
<script src="code/chapter/15_game.js"></script>
<script src="code/game_levels.js"></script>

<link rel="stylesheet" href="css/game.css">

<body>
<script>
  function runGame(plans, Display) {
    function startLevel(n, lives) {
      runLevel(new Level(plans[n]), Display, function(status) {
        if (status == "lost") {
          if (lives > 0) {
            startLevel(n, lives - 1);
          } else {
            console.log("Game over");
            startLevel(0, 3);
          }
        } else if (n < plans.length - 1) {
          startLevel(n + 1, lives);
        } else {
          console.log("You win!");
        }
      });
    }
    startLevel(0, 3);
  }
  runGame(GAME_LEVELS, DOMDisplay);
</script>
</body>
