
/* Proposta di delbio */

function min (a, b) {
  if (a <= b) return a;
  return b;
}
console.log(min(0, 10));
// → 0
console.log(min(0, -10));
// → -10


/* Proposta del libro */

function min(a, b) {
  if (a < b)
    return a;
  else
    return b;
}

console.log(min(0, 10));
// → 0
console.log(min(0, -10));
// → -10
