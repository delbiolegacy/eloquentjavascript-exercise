/* Proposta di delbio */

// Your code here.
function range(start, end) {
  var result = [];
  var step = arguments.length === 3 && arguments[2] !== 0 ? arguments[2] : 1;
  for (var i = start; step < 0 ? i >= end : i <= end; i+=step)
    result.push(i);
  return result;
}
function sum (numbers) {
  var result = 0;
  for (var i= 0; i < numbers.length; i++)
	  result += numbers[i];
  return result;
}
console.log(range(1, 10,0));
// → [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(5, 2, -1));
// → [5, 4, 3, 2]
console.log(sum(range(1, 10)));
// → 55

/* Proposta del libro */

function range(start, end, step) {
  if (step == null) step = 1;
  var array = [];

  if (step > 0) {
    for (var i = start; i <= end; i += step)
      array.push(i);
  } else {
    for (var i = start; i >= end; i += step)
      array.push(i);
  }
  return array;
}

function sum(array) {
  var total = 0;
  for (var i = 0; i < array.length; i++)
    total += array[i];
  return total;
}

console.log(range(1, 10))
// → [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(5, 2, -1));
// → [5, 4, 3, 2]
console.log(sum(range(1, 10)));
// → 55
