
/* Proposta di delbio */

function reverseArray(array){
  var output = [];
  for ( var i = array.length -1 ; i >= 0; i--)
    output.push(array[i]);
  return output;
}

// ho usato il suggerimento per prendere indici e swap
// ma non ho guardato la soluzione

function reverseArrayInPlace(array){
  var iterations = Math.floor(array.length/2)
  for (var li = 0; li < iterations; li++){
    var ri = array.length - 1 - li;
    var swap = array[li];
    array[li] = array[ri];
    array[ri] = swap;
  }
}

/* proposta del libro */

function reverseArray(array) {
  var output = [];
  for (var i = array.length - 1; i >= 0; i--)
    output.push(array[i]);
  return output;
}

function reverseArrayInPlace(array) {
  for (var i = 0; i < Math.floor(array.length / 2); i++) {
    var old = array[i];
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = old;
  }
  return array;
}

console.log(reverseArray(["A", "B", "C"]));
// → ["C", "B", "A"];
var arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);
// → [5, 4, 3, 2, 1]
