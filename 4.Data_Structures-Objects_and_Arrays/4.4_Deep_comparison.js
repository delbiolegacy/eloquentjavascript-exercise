/* proposta di delbio */

/*
 * Ho imbrogliato e ho guardato la soluzione dopo
 * aver visto gli aiuti.
 * Ho fatto delle prove, ma senza successo, oggi non ero molto
 * motivato.
 * Dopo aver scoperto che se si passa il mouse sopra download it
 * viene mostrata la soluzione nell'alt, mi sono arreso
 * a leggere la soluzione.
 */

/* proposta del libro */

function deepEqual(a, b) {
  if (a === b) return true;

  if (a == null || typeof a != "object" ||
      b == null || typeof b != "object")
    return false;

  var propsInA = 0, propsInB = 0;

  for (var prop in a)
    propsInA += 1;

  for (var prop in b) {
    propsInB += 1;
    if (!(prop in a) || !deepEqual(a[prop], b[prop]))
      return false;
  }

  return propsInA == propsInB;
}

var obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true
