[eloquentjavascript-book] esercizi
-------------------------------

L'obbiettivo e' quello di raccogliere le soluzioni
degli esercizi proposti per poter confrontare la mia
soluzione con quella del libro.

#Code Sandbox

- apri la [code-sandbox]
- scegli il capitolo
- scegli l'esercizio
- inserisci il tuo codice
- eseguilo
- copiati la soluzione
- confronta la tua soluzione con quella proposta dal libro
- aggiungi le due soluzioni in un file js di questo progetto rispettando la struttura dei capitoli e esercizi

[code-sandbox]:http://eloquentjavascript.net/code/
[eloquentjavascript-book]:http://eloquentjavascript.net/
